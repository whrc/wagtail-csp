.. image:: https://badge.fury.io/py/wagtail-csp.svg
   :target: https://pypi.org/project/wagtail-csp/


============
Wagtail-CSP
============

Waigtail-CSP adds Content-Security-Policy_ headers to a Wagtail site and allows them to be modifiable within admin.

The code lives on GitLab_, where you can report Issues_. It is a fork of the Mozilla project Django-CSP_.

This project is not endorsed or promoted by Django-CSP or its contributors.


.. _Content-Security-Policy: http://www.w3.org/TR/CSP/
.. _Django-CSP: https://github.com/mozilla/django-csp
.. _Issues: https://gitlab.com/womens-declaration/wagtail-csp/-/issues
.. _GitLab: https://gitlab.com/womens-declaration/wagtail-csp
